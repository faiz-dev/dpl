import React from 'react';
import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import 'react-pro-sidebar/dist/css/styles.css';
import FeatherIcon from 'feather-icons-react';

export default function NavBar() {
  return (
    <div>
      <ProSidebar>
      <Menu iconShape="square">
        <MenuItem icon={<FeatherIcon icon="menu" />}>Dashboard</MenuItem>
        <SubMenu title="Components" icon={<FeatherIcon icon="menu" />}>
          <MenuItem>Component 1</MenuItem>
          <MenuItem>Component 2</MenuItem>
        </SubMenu>
      </Menu>
    </ProSidebar>
    </div>
  )
}
