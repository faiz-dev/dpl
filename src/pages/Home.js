import React from 'react';
import Card from 'react-bootstrap/Card';
import { FaUsers, FaUser, FaStethoscope, FaCheck } from 'react-icons/fa';

export default function Home() {
  return (
    <div className="row justify-content-between">
      <div className='col-md-3'>
        <Card style={{backgroundColor: '#1E6170', marginTop: '1%'}}>
          <Card.Body>
            <div style={{columnCount: 2}}>
              <div style={{marginTop: '4%'}}>
                <center><FaUsers size={45} color={'white'} /></center>
              </div>
              <div style={{marginTop: '5%'}}>
                <div>
                  <center style={{color: '#ffffff', fontSize: 16, fontWeight: 'bolder', fontStyle: 'italic'}}>10</center>
                </div>
                <div style={{marginTop: '3%'}}>
                  <center style={{color: '#ffffff', fontSize: 14, fontWeight: '400', fontStyle: 'italic', fontFamily: 'seoge'}}>Total Patients</center>
                </div>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>

      <div className='col-md-3'>
        <Card style={{backgroundColor: '#1E6170', marginTop: '1%'}}>
          <Card.Body>
            <div style={{columnCount: 2}}>
              <div style={{marginTop: '4%'}}>
                <center><FaUser size={40} color={'white'} /></center>
              </div>
              <div style={{marginTop: '5%'}}>
                <div>
                  <center style={{color: '#ffffff', fontSize: 16, fontWeight: 'bolder', fontStyle: 'italic'}}>0</center>
                </div>
                <div style={{marginTop: '3%'}}>
                  <center style={{color: '#ffffff', fontSize: 14, fontWeight: '400', fontStyle: 'italic', fontFamily: 'seoge'}}>New Patients</center>
                </div>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>

      <div className='col-md-3'>
        <Card style={{backgroundColor: '#1E6170', marginTop: '1%'}}>
          <Card.Body>
            <div style={{columnCount: 2}}>
              <div style={{marginTop: '5%'}}>
                <center><FaStethoscope size={45} color={'white'} /></center>
              </div>
              <div style={{marginTop: '5%'}}>
                <div>
                  <center style={{color: '#ffffff', fontSize: 16, fontWeight: 'bolder', fontStyle: 'italic'}}>15</center>
                </div>
                <div style={{marginTop: '3%'}}>
                  <center style={{color: '#ffffff', fontSize: 14, fontWeight: '400', fontStyle: 'italic', fontFamily: 'seoge'}}>Diagnosis Pending</center>
                </div>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>

      <div className='col-md-3'>
        <Card style={{backgroundColor: '#1E6170', marginTop: '1%'}}>
          <Card.Body>
            <div style={{columnCount: 2}}>
              <div style={{marginTop: '4%'}}>
                <center><FaCheck size={45} color={'white'} /></center>
              </div>
              <div style={{marginTop: '5%'}}>
                <div>
                  <center style={{color: '#ffffff', fontSize: 16, fontWeight: 'bolder', fontStyle: 'italic'}}>5</center>
                </div>
                <div style={{marginTop: '3%'}}>
                  <center style={{color: '#ffffff', fontSize: 14, fontWeight: '400', fontStyle: 'italic', fontFamily: 'seoge'}}>Predictions Made</center>
                </div>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>
    </div>
  )
}
