import React from 'react';
import { useIntl } from 'react-intl';
import useWindowDimensions from './Layout/Dimensions';
// import Switch from 'react-switch';
// import Header from './Header';
import { FaBars, FaUserAlt } from 'react-icons/fa';
// import reactLogo from '../assets/logo.svg';
import Home from './Home';

const Main = ({
  collapsed,
  rtl,
  image,
  handleToggleSidebar,
  handleCollapsedChange,
  handleRtlChange,
  handleImageChange,
}) => {
  const { height, width } = useWindowDimensions();
  const intl = useIntl();
  return (
    <main style={{ width: {width}}}>
      <header style={{height: '5%'}}>
        <div className="btn-toggle" style={{backgroundColor: '#135571'}} onClick={() => handleToggleSidebar(true)}>
          <FaBars />
        </div>
        {/* <div className="float-right" style={{backgroundColor: 'black', marginBottom: '12%'}}>
          <FaUserAlt className="float-right" size={27} color={'#135571'} />
        </div> */}
      </header>

      <div>
        <Home />
      </div>
      {/* <div className="block ">
        <Switch
          height={16}
          width={30}
          checkedIcon={false}
          uncheckedIcon={false}
          onChange={handleCollapsedChange}
          checked={collapsed}
          onColor="#219de9"
          offColor="#bbbbbb"
        />
        <span> {intl.formatMessage({ id: 'collapsed' })}</span>
      </div> */}
      {/* <div className="block">
        <Switch
          height={16}
          width={30}
          checkedIcon={false}
          uncheckedIcon={false}
          onChange={handleRtlChange}
          checked={rtl}
          onColor="#219de9"
          offColor="#bbbbbb"
        />
        <span> {intl.formatMessage({ id: 'rtl' })}</span>
      </div> */}
      {/* <div className="block">
        <Switch
          height={16}
          width={30}
          checkedIcon={false}
          uncheckedIcon={false}
          onChange={handleImageChange}
          checked={image}
          onColor="#219de9"
          offColor="#bbbbbb"
        />
        <span> {intl.formatMessage({ id: 'image' })}</span>
      </div> */}

      {/* <footer>
        <small>
          © {new Date().getFullYear()} made with <FaHeart style={{ color: 'red' }} /> by -{' '}
          <a target="_blank" rel="noopener noreferrer" href="https://azouaoui.netlify.com">
            Mohamed Azouaoui
          </a>
        </small>
        <br />
        <div className="social-bagdes">
          <a href="https://github.com/azouaoui-med" target="_blank" rel="noopener noreferrer">
            <img
              alt="GitHub followers"
              src="https://img.shields.io/github/followers/azouaoui-med?label=github&style=social"
            />
          </a>
          <a href="https://twitter.com/azouaoui_med" target="_blank" rel="noopener noreferrer">
            <img
              alt="Twitter Follow"
              src="https://img.shields.io/twitter/follow/azouaoui_med?label=twitter&style=social"
            />
          </a>
        </div>
      </footer> */}
    </main>
  );
};

export default Main;