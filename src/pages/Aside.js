import React from 'react';
import { useIntl } from 'react-intl';
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from 'react-pro-sidebar';
import { FaHome, FaUsers, FaSignOutAlt, FaGithub, FaPlus, FaStethoscope, FaRegEdit, FaPlusSquare } from 'react-icons/fa';
import sidebarBg from '../assets/6880.jpg';
import sideBarLogo from '../assets/logoMDSS.png';

const Aside = ({ image, collapsed, rtl, toggled, handleToggleSidebar }) => {
  const intl = useIntl();
  return (
    <ProSidebar
      image={image ? sidebarBg : false}
      rtl={rtl}
      collapsed={collapsed}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}
    >
      <SidebarHeader>
        <div
          style={{
            padding: '15px',
            textTransform: 'uppercase',
            fontWeight: 'bold',
            fontSize: 14,
            letterSpacing: '1px',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
          }}
        >
          {/* {intl.formatMessage({ id: 'sidebarTitle' })} */}
          <div class="center-image">
            <img src={sideBarLogo} width={'75px'} />
          </div>
        </div>
      </SidebarHeader>

      <SidebarContent>
        <Menu iconShape="circle">
          <MenuItem
            icon={<FaHome />}
            // suffix={<span className="badge red">{intl.formatMessage({ id: 'new' })}</span>}
          >
            {/* {intl.formatMessage({ id: 'Home' })} */}
            Home
          </MenuItem>
          {/* <MenuItem icon={<FaGem />}> {intl.formatMessage({ id: 'components' })}</MenuItem> */}

          <SubMenu
            // suffix={<span className="badge yellow">3</span>}
            title={intl.formatMessage({ id: 'Patients' })}
            icon={<FaUsers />}
          >
            <MenuItem icon={<FaPlus />}>Add Patient</MenuItem>
            <MenuItem icon={<FaRegEdit />}>Edit Patient</MenuItem>
          </SubMenu>

          <SubMenu
            // prefix={<span className="badge gray">3</span>}
            title={intl.formatMessage({ id: 'Diagnose System' })}
            icon={<FaStethoscope />}
          >
            <MenuItem icon={<FaPlusSquare />}>Cardiac</MenuItem>
            <MenuItem icon={<FaPlusSquare />}>Diabetes</MenuItem>
            <MenuItem icon={<FaPlusSquare />}>Breast Cancer</MenuItem>
            <MenuItem icon={<FaPlusSquare />}>Ophthalmology</MenuItem>
          </SubMenu>
          {/* <SubMenu title={intl.formatMessage({ id: 'multiLevel' })} icon={<FaList />}>
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 1 </MenuItem>
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 2 </MenuItem>
            <SubMenu title={`${intl.formatMessage({ id: 'submenu' })} 3`}>
              <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.1 </MenuItem>
              <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.2 </MenuItem>
              <SubMenu title={`${intl.formatMessage({ id: 'submenu' })} 3.3`}>
                <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.3.1 </MenuItem>
                <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.3.2 </MenuItem>
                <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.3.3 </MenuItem>
              </SubMenu>
            </SubMenu>
          </SubMenu> */}
        </Menu>

        <Menu iconShape="circle">
          <MenuItem
            icon={<FaSignOutAlt />}
            // suffix={<span className="badge red">{intl.formatMessage({ id: 'new' })}</span>}
          >
            {/* {intl.formatMessage({ id: 'Home' })} */}
            Logout
          </MenuItem>
          {/* <MenuItem icon={<FaGem />}> {intl.formatMessage({ id: 'components' })}</MenuItem> */}
        </Menu>
      </SidebarContent>
{/* 
      <SidebarFooter style={{ textAlign: 'center' }}>
        <div
          className="sidebar-btn-wrapper"
          style={{
            padding: '20px 24px',
          }}
        >
          <a
            href="https://github.com/azouaoui-med/react-pro-sidebar"
            target="_blank"
            className="sidebar-btn"
            rel="noopener noreferrer"
          >
            <FaGithub />
            <span style={{ whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden' }}>
              {intl.formatMessage({ id: 'viewSource' })}
            </span>
          </a>
        </div>
      </SidebarFooter> */}
    </ProSidebar>
  );
};

export default Aside;